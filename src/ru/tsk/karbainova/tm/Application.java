package ru.tsk.karbainova.tm;

import static ru.tsk.karbainova.tm.constant.TerminalConst.*;

public class Application {

    public static void main(String[] args) {
        displayWelcome();
        run(args);
    }

    private static void displayWelcome() {
        System.out.println("** WELCOME TO TASK MANAGER **");
    }

    private static void run(final String[] args) {
        if (args == null) return;
        if (args.length < 1) return;
        final String param = args[0];
        if (CMD_HELP.equals(param)) displayHelp();
        if (CMD_VERSION.equals(param)) displayVersion();
        if (CMD_ABOUT.equals(param)) displayAbout();
    }

    private static void displayHelp() {
        System.out.println(CMD_VERSION + " - Display program version. ");
        System.out.println(CMD_ABOUT + " - Display developer info. ");
        System.out.println(CMD_HELP + " - Display list of terminal commands. ");
    }

    private static void displayVersion() {
        System.out.println("1.0.0");
    }

    private static void displayAbout() {
        System.out.println("Developer: Mriya Karbainova");
        System.out.println("E-mail: mariya@karbainova");
    }
}
